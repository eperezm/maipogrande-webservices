﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using CapaConexion;
using Oracle.DataAccess.Client;
using System.Data;

namespace CapaNegocio
{
   public class NegocioContrato
    {
        Conexion cn;
        OracleCommand comando;
        public void InsertarContrato(DtoContrato contrato)
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_CONTRATO_AGREGAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = (contrato.Empresa_idempresa);
            comando.Parameters.Add("v_fechainicio", OracleDbType.Date).Value = (contrato.Fechainicio);
            comando.Parameters.Add("v_fechainicio", OracleDbType.Date).Value = (contrato.Fechafin);
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = (contrato.Estado);

            try
            {
                comando.ExecuteNonQuery();
                //  MessageBox.Show("Se agrego correctamente");
                cn.cerrarConexion();
            }
            catch (Exception)
            {
                //  MessageBox.Show(ex.ToString(), "No se agrego");
            }

        }

        public void EditarContrato (DtoContrato contrato)
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_CONTRATO_EDITAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_idcontrato", OracleDbType.Int32).Value = (contrato.Idcontrato);
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = (contrato.Empresa_idempresa);
            comando.Parameters.Add("v_fechainicio", OracleDbType.Date).Value = (contrato.Fechainicio);
            comando.Parameters.Add("v_fechainicio", OracleDbType.Date).Value = (contrato.Fechafin);
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = (contrato.Estado);

            try
            {
                comando.ExecuteNonQuery();
                cn.cerrarConexion();
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }

        public DataTable ListarContrato()
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_LISTAR_CONTRATO_PRODUCTOR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("C_CONTRATO_PRODUCTOR", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            return tabla;
        }
        
    }
}
