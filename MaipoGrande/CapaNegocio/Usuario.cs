﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDTO;
using CapaConexion;
using Oracle.DataAccess.Client;


namespace CapaNegocio
{
    public class NegocioUsuario
    {

        Conexion cn;
        OracleCommand comando;
        public void insertarUsuario(DtoUsuario usuario)
           
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_USUARIO_AGREGAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_rutusuario", OracleDbType.Int32).Value =(usuario.Rutusuario);
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = usuario.Empresa_idempresa;
            comando.Parameters.Add("v_rol_idrol", OracleDbType.Int32).Value = usuario.Rol_idrol;
            comando.Parameters.Add("v_ciudad_idciudad", OracleDbType.Int32).Value = usuario.Ciudad_idciudad;
            comando.Parameters.Add("v_username", OracleDbType.Varchar2).Value = usuario.Username;
            comando.Parameters.Add("v_nombre", OracleDbType.Varchar2).Value = usuario.Nombre;
            comando.Parameters.Add("v_apellido", OracleDbType.Varchar2).Value = usuario.Apellido;
            comando.Parameters.Add("v_estado", OracleDbType.Int32).Value = usuario.Estado;
            comando.Parameters.Add("v_correo", OracleDbType.Varchar2).Value = usuario.Correo;
            comando.Parameters.Add("v_direccion", OracleDbType.Varchar2).Value = usuario.Direccion;
            comando.Parameters.Add("v_fono", OracleDbType.Varchar2).Value = usuario.Fono;
            comando.Parameters.Add("v_contrasena", OracleDbType.Varchar2).Value = usuario.Contrasena;
            try
            {
                comando.ExecuteNonQuery();
              //  MessageBox.Show("Se agrego correctamente");
                cn.cerrarConexion();
            }
            catch (Exception)
            {
              //  MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }


        public void EditarUsuario(DtoUsuario usuario)
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_USUARIO_EDITAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_rutusuario", OracleDbType.Int32).Value = (usuario.Rutusuario);
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = usuario.Empresa_idempresa;
            comando.Parameters.Add("v_rol_idrol", OracleDbType.Int32).Value = usuario.Rol_idrol;
            comando.Parameters.Add("v_ciudad_idciudad", OracleDbType.Int32).Value = usuario.Ciudad_idciudad;
            comando.Parameters.Add("v_username", OracleDbType.Varchar2).Value = usuario.Username;
            comando.Parameters.Add("v_nombre", OracleDbType.Varchar2).Value = usuario.Nombre;
            comando.Parameters.Add("v_apellido", OracleDbType.Varchar2).Value = usuario.Apellido;
            comando.Parameters.Add("v_estado", OracleDbType.Int32).Value = usuario.Estado;
            comando.Parameters.Add("v_correo", OracleDbType.Varchar2).Value = usuario.Correo;
            comando.Parameters.Add("v_direccion", OracleDbType.Varchar2).Value = usuario.Direccion;
            comando.Parameters.Add("v_fono", OracleDbType.Varchar2).Value = usuario.Fono;
            comando.Parameters.Add("v_contrasena", OracleDbType.Varchar2).Value = usuario.Contrasena;



            try
            {
                comando.ExecuteNonQuery();
              //  MessageBox.Show("Se ELIMINO correctamente");
                cn.cerrarConexion();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }



        public void EliminarUsuario(DtoUsuario usuario)
        { 
            cn = new Conexion();
            comando = new OracleCommand("PS_USUARIO_ELIMINAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_rutusuario", OracleDbType.Int32).Value = (usuario.Rutusuario);

            try
            {
                comando.ExecuteNonQuery();
               // MessageBox.Show("Se ELIMINO correctamente");
                cn.cerrarConexion();
            }
            catch (Exception ex)
            {
              //  MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }

        public DataTable ListarUsuario()
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_USUARIO_LISTAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            

            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            return tabla;
            
        }
    }
}
