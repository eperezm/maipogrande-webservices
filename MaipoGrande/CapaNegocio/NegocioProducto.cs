﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CapaDTO;
using CapaConexion;
using Oracle.DataAccess.Client;

namespace CapaNegocio
{
   public class NegocioProducto
    {
        Conexion cn;
        OracleCommand comando;
        public void insertarProducto(DtoProducto producto)
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_PRODUCTO_AGREGAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_idproducto", OracleDbType.Int32).Value = producto.Idproducto;
            comando.Parameters.Add("v_idsubcategoria", OracleDbType.Int32).Value = producto.Idsubcategoria;
            comando.Parameters.Add("v_bodega_idbodega", OracleDbType.Int32).Value = producto.Bodega_idbodega;
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = producto.Empresa_idempresa;
            comando.Parameters.Add("v_totalkilos", OracleDbType.Int32).Value = producto.Totalkilos;
            comando.Parameters.Add("v_fechacosecha", OracleDbType.Date).Value = producto.Fechacosecha;
            try
            {
                comando.ExecuteNonQuery();
                //  MessageBox.Show("Se agrego correctamente");
                cn.cerrarConexion();
            }
            catch (Exception)
            {
                //  MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }

        public void editarProducto(DtoProducto producto)
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_PRODUCTO_EDITAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_idproducto", OracleDbType.Int32).Value = producto.Idproducto;
            comando.Parameters.Add("v_idsubcategoria", OracleDbType.Int32).Value = producto.Idsubcategoria;
            comando.Parameters.Add("v_bodega_idbodega", OracleDbType.Int32).Value = producto.Bodega_idbodega;
            comando.Parameters.Add("v_empresa_idempresa", OracleDbType.Int32).Value = producto.Empresa_idempresa;
            comando.Parameters.Add("v_totalkilos", OracleDbType.Int32).Value = producto.Totalkilos;
            comando.Parameters.Add("v_fechacosecha", OracleDbType.Date).Value = producto.Fechacosecha;
            try
            {
                comando.ExecuteNonQuery();
                //  MessageBox.Show("Se agrego correctamente");
                cn.cerrarConexion();
            }
            catch (Exception)
            {
                //  MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }

        public void eliminarProducto(DtoProducto producto)
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_PRODUCTO_ELIMINAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("v_idproducto", OracleDbType.Int32).Value = producto.Idproducto;
            try
            {
                comando.ExecuteNonQuery();
                //  MessageBox.Show("Se agrego correctamente");
                cn.cerrarConexion();
            }
            catch (Exception)
            {
                //  MessageBox.Show(ex.ToString(), "No se agrego");
            }
        }

        public DataTable ListarProducto()
        {
            cn = new Conexion();
            comando = new OracleCommand("PS_PRODUCTO_LISTAR", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("PRODUCTO", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

            OracleDataAdapter adaptador = new OracleDataAdapter();
            adaptador.SelectCommand = comando;
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);
            return tabla;
        }
    }
}
