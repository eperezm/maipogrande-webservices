﻿
namespace Pruebaa
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnConectar = new System.Windows.Forms.Button();
            this.TxtnombreAgregar = new System.Windows.Forms.TextBox();
            this.TxtEliminarNombre = new System.Windows.Forms.TextBox();
            this.BtnEliminar = new System.Windows.Forms.Button();
            this.BtnEditar = new System.Windows.Forms.Button();
            this.TxtNombreEditar = new System.Windows.Forms.TextBox();
            this.TxtidEditar = new System.Windows.Forms.TextBox();
            this.DtgListar = new System.Windows.Forms.DataGridView();
            this.BtnListar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DtgListar)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnConectar
            // 
            this.BtnConectar.Location = new System.Drawing.Point(271, 26);
            this.BtnConectar.Name = "BtnConectar";
            this.BtnConectar.Size = new System.Drawing.Size(75, 23);
            this.BtnConectar.TabIndex = 0;
            this.BtnConectar.Text = "Agregar";
            this.BtnConectar.UseVisualStyleBackColor = true;
            this.BtnConectar.Click += new System.EventHandler(this.BtnConectar_Click);
            // 
            // TxtnombreAgregar
            // 
            this.TxtnombreAgregar.Location = new System.Drawing.Point(69, 26);
            this.TxtnombreAgregar.Name = "TxtnombreAgregar";
            this.TxtnombreAgregar.Size = new System.Drawing.Size(144, 20);
            this.TxtnombreAgregar.TabIndex = 1;
            // 
            // TxtEliminarNombre
            // 
            this.TxtEliminarNombre.Location = new System.Drawing.Point(69, 80);
            this.TxtEliminarNombre.Name = "TxtEliminarNombre";
            this.TxtEliminarNombre.Size = new System.Drawing.Size(144, 20);
            this.TxtEliminarNombre.TabIndex = 2;
            // 
            // BtnEliminar
            // 
            this.BtnEliminar.Location = new System.Drawing.Point(271, 77);
            this.BtnEliminar.Name = "BtnEliminar";
            this.BtnEliminar.Size = new System.Drawing.Size(75, 23);
            this.BtnEliminar.TabIndex = 3;
            this.BtnEliminar.Text = "Eliminar";
            this.BtnEliminar.UseVisualStyleBackColor = true;
            this.BtnEliminar.Click += new System.EventHandler(this.BtnEliminar_Click);
            // 
            // BtnEditar
            // 
            this.BtnEditar.Location = new System.Drawing.Point(410, 141);
            this.BtnEditar.Name = "BtnEditar";
            this.BtnEditar.Size = new System.Drawing.Size(75, 23);
            this.BtnEditar.TabIndex = 4;
            this.BtnEditar.Text = "Editar";
            this.BtnEditar.UseVisualStyleBackColor = true;
            this.BtnEditar.Click += new System.EventHandler(this.BtnEditar_Click);
            // 
            // TxtNombreEditar
            // 
            this.TxtNombreEditar.Location = new System.Drawing.Point(229, 141);
            this.TxtNombreEditar.Name = "TxtNombreEditar";
            this.TxtNombreEditar.Size = new System.Drawing.Size(144, 20);
            this.TxtNombreEditar.TabIndex = 5;
            // 
            // TxtidEditar
            // 
            this.TxtidEditar.Location = new System.Drawing.Point(69, 141);
            this.TxtidEditar.Name = "TxtidEditar";
            this.TxtidEditar.Size = new System.Drawing.Size(141, 20);
            this.TxtidEditar.TabIndex = 6;
            // 
            // DtgListar
            // 
            this.DtgListar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgListar.Location = new System.Drawing.Point(49, 211);
            this.DtgListar.Name = "DtgListar";
            this.DtgListar.Size = new System.Drawing.Size(472, 150);
            this.DtgListar.TabIndex = 7;
            // 
            // BtnListar
            // 
            this.BtnListar.Location = new System.Drawing.Point(445, 390);
            this.BtnListar.Name = "BtnListar";
            this.BtnListar.Size = new System.Drawing.Size(75, 23);
            this.BtnListar.TabIndex = 8;
            this.BtnListar.Text = "Listar";
            this.BtnListar.UseVisualStyleBackColor = true;
            this.BtnListar.Click += new System.EventHandler(this.BtnListar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.BtnListar);
            this.Controls.Add(this.DtgListar);
            this.Controls.Add(this.TxtidEditar);
            this.Controls.Add(this.TxtNombreEditar);
            this.Controls.Add(this.BtnEditar);
            this.Controls.Add(this.BtnEliminar);
            this.Controls.Add(this.TxtEliminarNombre);
            this.Controls.Add(this.TxtnombreAgregar);
            this.Controls.Add(this.BtnConectar);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DtgListar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnConectar;
        private System.Windows.Forms.TextBox TxtnombreAgregar;
        private System.Windows.Forms.TextBox TxtEliminarNombre;
        private System.Windows.Forms.Button BtnEliminar;
        private System.Windows.Forms.Button BtnEditar;
        private System.Windows.Forms.TextBox TxtNombreEditar;
        private System.Windows.Forms.TextBox TxtidEditar;
        private System.Windows.Forms.DataGridView DtgListar;
        private System.Windows.Forms.Button BtnListar;
    }
}

