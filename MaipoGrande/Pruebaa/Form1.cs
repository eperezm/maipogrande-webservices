﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Data.OracleClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaConexion;
using Oracle.DataAccess.Client;



namespace Pruebaa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

       // [Obsolete]
        Conexion cn;
        OracleCommand comando;
        
        private void BtnConectar_Click(object sender, EventArgs e)
        {
            

            cn = new Conexion();
            comando = new OracleCommand("AGREGAR_PAIS",cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("NOM" , OracleDbType.Varchar2).Value = TxtnombreAgregar.Text;
            try
            {
                comando.ExecuteNonQuery();
                MessageBox.Show("Se agrego correctamente");
                cn.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(),"No se agrego");
            }
            //cn.Close();
        }
        
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            cn = new Conexion();
            comando = new OracleCommand("ELIMINAR_PAIS", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("idd", OracleDbType.Int64).Value = Convert.ToInt32(TxtEliminarNombre.Text);

            try
            {
                comando.ExecuteNonQuery();
                MessageBox.Show("Se ELIMINO correctamente");
                cn.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se agrego");
            }
            
        }

        private void BtnEditar_Click(object sender, EventArgs e)
        {
            cn = new Conexion();
            comando = new OracleCommand("EDITAR_PAIS", cn.regresaConexion());
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Add("idd", OracleDbType.Int32).Value = Convert.ToInt32(TxtidEditar.Text);
            comando.Parameters.Add("nom", OracleDbType.Varchar2).Value = TxtNombreEditar.Text;

            try
            {
                comando.ExecuteNonQuery();
                MessageBox.Show("Se EDITO correctamente");
                cn.cerrarConexion();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "No se agrego");
            }

            
        }

        private void BtnListar_Click(object sender, EventArgs e)
        {
                cn = new Conexion();
                comando = new OracleCommand("LISTAR_PAIS", cn.regresaConexion());
                comando.CommandType = CommandType.StoredProcedure;
                comando.Parameters.Add("PAISES", OracleDbType.RefCursor).Direction = ParameterDirection.Output;

                OracleDataAdapter adaptador = new OracleDataAdapter();
                adaptador.SelectCommand = comando;
                DataTable tabla = new DataTable();
                adaptador.Fill(tabla);
                DtgListar.DataSource = tabla;  


            
        }
        
    }
}
