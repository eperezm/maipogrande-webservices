﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;




namespace Pruebaa
{
    public partial class Prueba2 : Form
    {
        public Prueba2()
        {
            InitializeComponent();
        }

        
        private void btnguardar_Click(object sender, EventArgs e)
        {
            try
            {
                ServicesAgregarUsuario.WebServicesUsuarioSoapClient auxserviceusuario = new ServicesAgregarUsuario.WebServicesUsuarioSoapClient();
                ServicesAgregarUsuario.DtoUsuario usuario = new ServicesAgregarUsuario.DtoUsuario();

                usuario.Rutusuario = Convert.ToInt32(this.Txtrut.Text);
                usuario.Empresa_idempresa = Convert.ToInt32(txtempresa.Text);
                usuario.Rol_idrol = Convert.ToInt32(txtrol.Text);
                usuario.Ciudad_idciudad = Convert.ToInt32(txtciudad.Text);
                usuario.Username = txtnombreusuario.Text;
                usuario.Nombre = txtnombre.Text;
                usuario.Apellido = txtapellido.Text;
                usuario.Estado = Convert.ToInt32(txtestado.Text);
                usuario.Correo = txtcorreo.Text;
                usuario.Direccion = txtdirecion.Text;
                usuario.Fono = txtfono.Text;
                usuario.Contrasena = txtcontrasena.Text;


                auxserviceusuario.insertarUsuarioService(usuario);
                MessageBox.Show("Datos guardados");


            }
            catch (Exception ex)
            {
                MessageBox.Show("Datos no guardados" + ex.Message,"Mensaje de sistema");
                throw;
            }

        }
    }
}
