﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaDTO;
using CapaNegocio;
using Oracle.DataAccess.Client;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServicesUsuario
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServicesUsuario : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarUsuarioService(DtoUsuario usuario)
        {
            NegocioUsuario nu = new NegocioUsuario();
            nu.insertarUsuario(usuario);
        }


        [WebMethod]
        public void EditarUsuarioService(DtoUsuario usuario)
        {
            NegocioUsuario nu = new NegocioUsuario();
            nu.EditarUsuario(usuario);
        }


        [WebMethod]
        public void EliminarUsuarioService(DtoUsuario usuario)
        {
            NegocioUsuario nu = new NegocioUsuario();
            nu.EliminarUsuario(usuario);
        }

        [WebMethod]
        public DataTable ListarUsuarioService()
        {
            NegocioUsuario nu = new NegocioUsuario();
            return nu.ListarUsuario();
        }



    }
}
