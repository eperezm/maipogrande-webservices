﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaDTO;
using CapaNegocio;
using Oracle.DataAccess.Client;
using System.Data;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServicesContrato
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServicesContrato : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarContratoService(DtoContrato contrato)
        {
            NegocioContrato InsertarContrato = new NegocioContrato();
            InsertarContrato.InsertarContrato(contrato);
        }

        [WebMethod]
        public void editarContratoService(DtoContrato contrato)
        {
            NegocioContrato EditarContrato = new NegocioContrato();
            EditarContrato.EditarContrato(contrato);
        }

        [WebMethod]
        public DataTable listarContratoService()
        {
            NegocioContrato listContrato = new NegocioContrato();
            return listContrato.ListarContrato();
        }
    }
}
