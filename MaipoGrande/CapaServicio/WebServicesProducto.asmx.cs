﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using CapaDTO;
using CapaNegocio;
using Oracle.DataAccess.Client;
using System.Data;

namespace CapaServicio
{
    /// <summary>
    /// Descripción breve de WebServicesProducto
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class WebServicesProducto : System.Web.Services.WebService
    {

        [WebMethod]
        public void insertarProductoService(DtoProducto producto)
        {
            NegocioProducto np = new NegocioProducto();
            np.insertarProducto(producto);
        }
        [WebMethod]
        public void editarProductoService(DtoProducto producto)
        {
            NegocioProducto np = new NegocioProducto();
            np.editarProducto(producto);
        }
        [WebMethod]
        public void eliminarProductoService(DtoProducto producto)
        {
            NegocioProducto np = new NegocioProducto();
            np.eliminarProducto(producto);
        }
        [WebMethod]
        public DataTable listarProductoService()
        {
            NegocioProducto np = new NegocioProducto();
            return np.ListarProducto();
        }
    }
}
