﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

 namespace CapaDTO
{
    public class DtoUsuario
    {
        private int rutusuario;
        private int empresa_idempresa;
        private int rol_idrol;
        private int ciudad_idciudad;
        private string username;
        private string nombre;
        private string apellido;
        private int estado;
        private string correo;
        private string direccion;
        private string fono;
        private string contrasena;

        public int Rutusuario { get => rutusuario; set => rutusuario = value; }
        public int Empresa_idempresa { get => empresa_idempresa; set => empresa_idempresa = value; }
        public int Rol_idrol { get => rol_idrol; set => rol_idrol = value; }
        public int Ciudad_idciudad { get => ciudad_idciudad; set => ciudad_idciudad = value; }
        public string Username { get => username; set => username = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public int Estado { get => estado; set => estado = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Fono { get => fono; set => fono = value; }
        public string Contrasena { get => contrasena; set => contrasena = value; }
    }
}
