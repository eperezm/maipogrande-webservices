﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class DtoEmpresa
    {

        private int idempresa;
        private int tipo_empresa_idtipoempresa;
        private string nombreempresa;
        private string direccionempresa;
        private string fonoempresa;
        private string correoempresa;
        private string intgiro;

        public int Idempresa { get => idempresa; set => idempresa = value; }
        public int Tipo_empresa_idtipoempresa { get => tipo_empresa_idtipoempresa; set => tipo_empresa_idtipoempresa = value; }
        public string Nombreempresa { get => nombreempresa; set => nombreempresa = value; }
        public string Direccionempresa { get => direccionempresa; set => direccionempresa = value; }
        public string Fonoempresa { get => fonoempresa; set => fonoempresa = value; }
        public string Correoempresa { get => correoempresa; set => correoempresa = value; }
        public string Intgiro { get => intgiro; set => intgiro = value; }
    }
}
