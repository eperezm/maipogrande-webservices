﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class DtoProducto
    {
        private int idproducto;
        private int idsubcategoria;
        private int bodega_idbodega;
        private string empresa_idempresa;
        private int totalkilos;
        private DateTime fechacosecha;

        public int Idproducto { get => idproducto; set => idproducto = value; }
        public int Idsubcategoria { get => idsubcategoria; set => idsubcategoria = value; }
        public int Bodega_idbodega { get => bodega_idbodega; set => bodega_idbodega = value; }
        public string Empresa_idempresa { get => empresa_idempresa; set => empresa_idempresa = value; }
        public int Totalkilos { get => totalkilos; set => totalkilos = value; }
        public DateTime Fechacosecha { get => fechacosecha; set => fechacosecha = value; }
    }
}
