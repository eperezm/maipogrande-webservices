﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDTO
{
    public class DtoContrato
    {
        private int idcontrato;
        private int empresa_idempresa;
        private DateTime fechainicio;
        private DateTime fechafin;
        private int estado;

        public int Idcontrato { get => idcontrato; set => idcontrato = value; }
        public int Empresa_idempresa { get => empresa_idempresa; set => empresa_idempresa = value; }
        public DateTime Fechainicio { get => fechainicio; set => fechainicio = value; }
        public DateTime Fechafin { get => fechafin; set => fechafin = value; }
        public int Estado { get => estado; set => estado = value; }
    }
}
